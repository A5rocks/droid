from __future__ import annotations

import hikari
import aiosqlite

db: aiosqlite.Connection = None
saved_user_ids: list[int] = []
embed_id = 0
bot = hikari.BotApp(token='TOKEN-CHAN')

# event msg id -> (msg id, chan id)
sus_msgs = {}

channel_ids = [
    132632676225122304,  # testing-[\]  (2.4M)
    113743192305827841,  # testing-^_`  (0.9M)
    117018340114825217,  # testing-voice (247K)
    119222314964353025,  # ultimate-shitposting (2.6M)
    318747893706129409,  # verification (66K)
    744813231452979220,  # verification-general (6K)
    716267531685462017,  # verification-testing (3.2K)
    374594284915261440,  # verification-2 (25K)
    476609041821335553,  # verification-sandbox (60K)
    734419877481152592,  # verification-sandbox-2 (57K)
    751849132557205554,  # verification-notes (8K)
]


@bot.listen()
async def connect_db(_event: hikari.StartingEvent):
    global db, saved_user_ids, embed_id
    db = await aiosqlite.connect('./messagedump.db')
    async with db.execute('SELECT id FROM users') as cursor:
        async for row in cursor:
            saved_user_ids.append(row[0])

    embed_id = (await (await db.execute('SELECT MAX(id) FROM embeds')).fetchone()) or (0,)
    embed_id = embed_id[0]

@bot.listen()
async def quit_db(_event: hikari.StoppingEvent):
    await db.close()


@bot.listen()
async def db_adder(event: hikari.GuildMessageCreateEvent):
    global db, saved_user_ids, embed_id
    if event.channel_id not in channel_ids:
        return

    if event.author_id not in saved_user_ids:
        saved_user_ids.append(event.author_id)
        author = event.author
        await db.execute("""
INSERT INTO users (id, username, discriminator, is_bot)
VALUES ($1, $2, $3, $4)
                        """, (author.id, author.username,
                              author.discriminator, author.is_bot))

    try:
        await db.execute("""
INSERT INTO messages (id, channel_id, author_id, content, time_sent,
                     time_edited, is_reply)
VALUES ($1, $2, $3, $4, $5, $6, $7)
                """,
                         (event.message_id, event.channel_id, event.author.id, event.content,
                          event.message.created_at, event.message.edited_timestamp, event.message.type == 19)
                         )
    except aiosqlite.IntegrityError:
        print('cry about it:', event.message_id, event.channel_id)
        return

        # i am crying about it ;w;

    # embeds table :(
    for embed in event.embeds:
        embed_id += 1
        await db.execute("""
INSERT INTO embeds (id, message_id, title, description, url, stamped, color,
image_url, thumbnail_url, video_url)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
                """,
                         (embed_id,
                          event.message_id,
                          embed.title,
                          embed.description,
                          embed.url,
                          embed.timestamp,
                          embed.color,
                          embed.image.url if embed.image else None,
                          embed.thumbnail.url if embed.thumbnail else None,
                          embed.video.url if embed.video else None)
                         )

        await db.executemany("""
INSERT INTO embed_fields (embed_id, name, value, inline)
VALUES ($1, $2, $3, $4)""",
                             [(embed_id, field.name, field.value, field.is_inline)
                              for field in embed.fields]
                             )

        if embed.footer:
            await db.execute("""
INSERT INTO embed_footers (embed_id, content, icon_url)
VALUES ($1, $2, $3)""",
                             (embed_id,
                              embed.footer.icon.url if embed.footer.icon else None,
                              embed.footer.text)
                             )

        if embed.provider:
            await db.execute("""
INSERT INTO embed_providers (embed_id, provider_name, provider_url)
VALUES ($1, $2, $3)""",
                             (embed_id,
                              embed.provider.name,
                              embed.provider.url)
                             )

        if embed.author:
            await db.execute("""
INSERT INTO embed_authors (embed_id, name, url, icon_url)
VALUES ($1, $2, $3, $4)""",
                             (embed_id,
                              embed.author.name,
                              embed.author.url,
                              embed.author.icon.url if embed.author.icon else None)
                             )


checked_channel_ids = [
    318747893706129409,  # verification (66K)
    744813231452979220,  # verification-general (6K)
    716267531685462017,  # verification-testing (3.2K)
    374594284915261440,  # verification-2 (25K)
    476609041821335553,  # verification-sandbox (60K)
    734419877481152592,  # verification-sandbox-2 (57K)
]


@bot.listen()
async def db_checker(event: hikari.GuildMessageCreateEvent):
    if event.channel_id not in checked_channel_ids or not event.message.content or not event.is_bot:
        return

    async with db.execute("""
SELECT messages.id, messages.channel_id FROM messages JOIN users ON users.id = messages.author_id
WHERE messages.content = $1 AND messages.author_id != $2 AND users.is_bot
LIMIT 5
        """,
        (event.message.content, event.author_id)) as cursor:
        lst = []
        async for row in cursor:
            lst.append((row[0], row[1]))

        sus_msgs[event.message_id] = lst

    if len(sus_msgs[event.message_id]) > 0:
        await event.message.add_reaction('doubt:585958700586369057')

@bot.listen()
async def sus_explainer(event: hikari.GuildReactionAddEvent):
    if not isinstance(event.emoji, hikari.CustomEmoji):
        return

    if event.member.is_bot:
        return 

    if event.emoji.id != 585958700586369057:
        return

    if event.channel_id not in checked_channel_ids:
        return

    result = ''
    for msg in sus_msgs.get(event.message_id, []):
        result += f'\n- <https://discord.com/channels/110373943822540800/{msg[1]}/{msg[0]}>\n'

    if result:
        await event.app.rest.create_message(event.channel_id, result.strip())
        del sus_msgs[event.message_id]

if __name__ == '__main__':
    bot.run()
