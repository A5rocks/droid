from __future__ import annotations

TOKEN = 'TOKEN-CHAN'

channel_ids = [
    '132632676225122304',  #testing-[\]  (2.4M)
    '113743192305827841',  #testing-^_`  (0.9M)
    '117018340114825217',  #testing-voice (247K)
    '119222314964353025',  #ultimate-shitposting (2.6M)
    '318747893706129409',  #verification (66K)
    '744813231452979220',  #verification-general (6K)
    '716267531685462017',  #verification-testing (3.2K)
    '374594284915261440',  #verification-2 (25K)
    '476609041821335553',  #verification-sandbox (60K)
    '734419877481152592',  #verification-sandbox-2 (57K)
    '751849132557205554',  #verification-notes (8K)
]

# eta assumes the message count is as listed above, and that the ratelimit is
# 1 request / 1 second. (aka, 100 messages per second).
eta = 2_600_000 / 100  # (seconds)

print(f'expected time taken: {eta / 60 / 60} hours')
input('are you sure you want to wait that long? just press enter.')

import sqlite3
import asyncio
import hikari

conn = sqlite3.connect('messagedump.db')
cursor = conn.cursor()

cursor.execute('PRAGMA foreign_keys = ON')

# users table
cursor.execute("""
CREATE TABLE IF NOT EXISTS users (
    id INTEGER NOT NULL PRIMARY KEY,
    username TEXT NOT NULL,
    discriminator TEXT NOT NULL,
    is_bot BOOLEAN NOT NULL
)
""")

# message table
# (guild id can be fetched from channel id & cached)
cursor.execute("""
CREATE TABLE IF NOT EXISTS messages (
    id INTEGER NOT NULL PRIMARY KEY,
    channel_id INTEGER NOT NULL,
    author_id INTEGER NOT NULL,
    content TEXT,
    time_sent DATETIME NOT NULL,
    time_edited DATETIME,
    is_reply BOOLEAN NOT NULL,
    FOREIGN KEY(author_id) REFERENCES users(id)
)
""")

# embed table
cursor.execute("""
CREATE TABLE IF NOT EXISTS embeds (
    id INTEGER NOT NULL PRIMARY KEY,
    message_id INTEGER NOT NULL,
    title TEXT,
    type TEXT,
    description TEXT,
    url TEXT,
    stamped DATETIME,  -- corresponds to `timestamp`
    color INTEGER,
    -- these being here shouldn't cause too much memory overhead... hopefully?
    image_url TEXT,
    thumbnail_url TEXT,
    video_url TEXT,
    FOREIGN KEY(message_id) REFERENCES messages(id)
)
""")

# embed fields table :/ (arrays wen eta)
cursor.execute("""
CREATE TABLE IF NOT EXISTS embed_fields (
    embed_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    value TEXT NOT NULL,
    inline BOOLEAN,
    FOREIGN KEY(embed_id) REFERENCES embeds(id)
)
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS embed_footers (
    embed_id INTEGER NOT NULL,
    content TEXT,
    icon_url TEXT,
    FOREIGN KEY(embed_id) REFERENCES embeds(id)
)
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS embed_providers (
    embed_id INTEGER NOT NULL,
    provider_name TEXT,
    provider_url TEXT,
    FOREIGN KEY(embed_id) REFERENCES embeds(id)
)
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS embed_authors (
    embed_id INTEGER NOT NULL,
    name TEXT,
    url TEXT,
    icon_url TEXT,
    FOREIGN KEY(embed_id) REFERENCES embeds(id)
)
""")

conn.commit()

rest = hikari.RESTApp()
saved_users = [n[0] for n in cursor.execute('SELECT id FROM users').fetchall()]
embed_id = cursor.execute('SELECT MAX(id) FROM embeds').fetchone() or (0,)
embed_id = embed_id[0]

async def get_messages_after(
    channel_id: str,
    message_id: int,
    client
) -> list[hikari.Message]:
    msgs = client.fetch_messages(channel_id, after=message_id + 1)
    return await msgs.limit(100)

async def scrape(channel_id: str, client) -> None:
    global embed_id
    message_id = cursor.execute(
        'SELECT MAX(id) FROM messages WHERE channel_id = $1',
        (channel_id,)
    ).fetchone() or (0,)
    message_id = message_id[0]
    messages = await get_messages_after(channel_id, message_id, client)
    while messages:
        for message in messages:
            # user table
            if message.author.id not in saved_users:
                saved_users.append(message.author.id)

                cursor.execute("""
INSERT INTO users (id, username, discriminator, is_bot)
VALUES ($1, $2, $3, $4)
                """, (message.author.id, message.author.username,
                    message.author.discriminator, message.author.is_bot))
            try:
                # messages table
                cursor.execute("""
    INSERT INTO messages (id, channel_id, author_id, content, time_sent,
                            time_edited, is_reply)
    VALUES ($1, $2, $3, $4, $5, $6, $7)
                    """,
                    (message.id,
                    message.channel_id,
                    message.author.id,
                    message.content,
                    message.created_at,
                    message.edited_timestamp,
                    message.type == 19)
                )
            except sqlite3.IntegrityError:
                print('cry about it:', message.id, message.channel_id)
                continue
                # ;w; i am crying about it

            # embeds table :(
            for embed in message.embeds:
                embed_id += 1
                cursor.execute("""
INSERT INTO embeds (id, message_id, title, description, url, stamped, color,
image_url, thumbnail_url, video_url)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
                """,
                    (embed_id,
                    message.id,
                    embed.title,
                    embed.description,
                    embed.url,
                    embed.timestamp,
                    embed.color,
                    embed.image.url if embed.image else None,
                    embed.thumbnail.url if embed.thumbnail else None,
                    embed.video.url if embed.video else None)
                )

                cursor.executemany("""
INSERT INTO embed_fields (embed_id, name, value, inline)
VALUES ($1, $2, $3, $4)""",
                    [(embed_id, field.name, field.value, field.is_inline)
                    for field in embed.fields]
                )

                if embed.footer:
                    cursor.execute("""
INSERT INTO embed_footers (embed_id, content, icon_url)
VALUES ($1, $2, $3)""",
                        (embed_id,
                        embed.footer.icon.url if embed.footer.icon else None,
                        embed.footer.text)
                    )

                if embed.provider:
                    cursor.execute("""
INSERT INTO embed_providers (embed_id, provider_name, provider_url)
VALUES ($1, $2, $3)""",
                        (embed_id,
                        embed.provider.name,
                        embed.provider.url)
                    )

                if embed.author:
                    cursor.execute("""
INSERT INTO embed_authors (embed_id, name, url, icon_url)
VALUES ($1, $2, $3, $4)""",
                        (embed_id,
                        embed.author.name,
                        embed.author.url,
                        embed.author.icon.url if embed.author.icon else None)
                    )

        conn.commit()

        message_id = messages[-1].id
        messages = await get_messages_after(channel_id, message_id, client)

    print(f'done with {channel_id}!!')

async def main():
    async with rest.acquire(TOKEN, 'Bot') as client:
        await asyncio.gather(*[
            scrape(channel_id, client) for channel_id in channel_ids
        ])
        print('DONE!!')

asyncio.run(main())
